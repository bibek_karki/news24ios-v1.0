//
//  ViewController.swift
//  News24Nepal
//
//  Created by Bibek Karki on 7/2/16.
//  Copyright © 2016 appharu. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var homeActivityIndicator: UIActivityIndicatorView!
    
    func callNewsCategoryApi() {
        homeActivityIndicator.startAnimating()
        Alamofire.request(.GET, ApiUrls.newsCategoryUrl, parameters: nil, encoding: .JSON, headers: nil).responseJSON{ (response) in
            
            switch response.result{
                case .Success:
                    if let json = response.result.value{
                        print("news categories json \n: \(json)")
                        
                        NSUserDefaults.standardUserDefaults().setObject(json, forKey: "newsCategoriesJson")
                        self.homeActivityIndicator.stopAnimating()
                        return
                    }
            case .Failure(let error):
                    print(error)
                    self.homeActivityIndicator.stopAnimating()
                    
                    return
            }
        }
    }
    
    func callApi(){
        let newsCategory = NSUserDefaults.standardUserDefaults().objectForKey("newsCategoriesJson")
        if  newsCategory == nil {
            callNewsCategoryApi()
        }else{
            print("api not called, set from UserDefaults")
            print(newsCategory)
            
            self.homeActivityIndicator.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        callApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 
}

