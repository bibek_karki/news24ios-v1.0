//
//  ApiUrls.swift
//  News24Nepal
//
//  Created by Bibek Karki on 7/3/16.
//  Copyright © 2016 appharu. All rights reserved.
//

import Foundation

class ApiUrls {
    //url for news category
    static let newsCategoryUrl = "http://www.news24nepal.tv/wp-json/appharurest/v011/categories/"
    
    static let CONTENT_NEWS_DEFAULT_NUMBER_OF_ITEMS = 10
    
    //url for news list
    static func getNewsListItemUrl(newsCategoryId : Int, numberOfItems : Int) -> String{
        let newsListItemUrl =  "http://www.news24nepal.tv/wp-json/appharurest/v011/list-posts/ + \(newsCategoryId) " +
            "/" + "\(numberOfItems)"
        
        print(newsListItemUrl)
        return newsListItemUrl
    }
    
    //url for main activity
    static func getMainNewsUrl() -> String{
        return "http://www.news24nepal.tv/wp-json/appharurest/v011/catpost"
    }
    
    //url for notification
    static func getNotificationUrl(deviceId : String, registrationId : String) -> String{
        return "http://www.news24nepal.tv/wp-json/appharurest/v011/gcm/"+deviceId + "/" + registrationId
    }
    
    //url for single news
    static func getNewsDetailsUrl(id : String) -> String{
        return "http://www.news24nepal.tv/wp-json/appharurest/v011/post/" + id
    }
}

